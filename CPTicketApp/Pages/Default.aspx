﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link rel="Stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" type="text/css" />
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script src="../Scripts/jquery-ui-1.11.4.min.js"></script>
    <script src="../Scripts/datepicker-nl.js"></script>
    <script src="../Scripts/DatumFuncties.js"></script>
    <script src="../Scripts/Models/TicketViewModel.js"></script>
    <script src="../Scripts/Models/KlantViewModel.js"></script>
    <script src="../Scripts/Models/ContactViewModel.js"></script>
    <script src="../Scripts/Models/GroepViewModel.js"></script>
    <script src="../Scripts/Models/TicketStatusViewModel.js"></script>
    <script src="../Scripts/Models/TicketGarantieViewModel.js"></script>
    <script src="../Scripts/Models/TicketTypeViewModel.js"></script>
    <script src="../Scripts/Models/TicketUserViewModel.js"></script>
    <script src="../Scripts/jquery.dataTables.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.min.js"></script>
    <script src="../Scripts/jquery.tablesorter.widgets.min.js"></script>
    <script src="../Scripts/tinymce/js/tinymce_4.1.9_jquery/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="../Scripts/Map.js"></script>
    <script src="../Scripts/upload.js"></script>
    <script src="../Scripts/be.itc.sp.exception.js"></script>
    <script src="../Scripts/be.itc.sp.rest.fileupload.js"></script>
    <script src="../Scripts/be.itc.sp.editor.tinymce.js"></script>
    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Tickets
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div class="overlay Hidden"></div>
    <div>
        <p id="message">
            <!-- The following content will be replaced with the user name when you run the app - see App.js -->
            initializing...
        </p>
    </div>
    <nav>
        <ul>
            <li onclick="OpenKlantPopup();">Nieuwe klant</li>
            <li onclick="OpenContactPopup();">Nieuw contact</li>
            <li onclick="OpenNieuwTicketPopup();">Nieuw ticket</li>
        </ul>
    </nav>
    <div>
        <fieldset>
            <label for="zoekKlantZoekTxt">Zoek:</label>
            <input type="text" id="zoekKlantZoekTxt" />
            <span id="zoekResultaatLabel"></span>
            <label for="klantSelect">Klanten:</label>
            <select id="klantselect" onchange="klantSelect_Change();"></select>
        </fieldset>
    </div>
    <div class="detailsDiv">
        <h3>Klant details</h3>
        <div class="stap heigth280">
            <div class="rightDiv">
                <a onclick="BewerkKlant_Click();" id="bewerkKlantButton" class="Buttons Hidden">Bewerk klant</a>
            </div>
            <fieldset>
                <label class="labelFor" for="klantNaamLabel">Naam:</label>
                <span id="klantNaamLabel"></span>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="klantTelefoonLabel">Telefoon:</label>
                <span id="klantTelefoonLabel"></span>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="klantAdresLabel">Adres:</label>
                <span id="klantAdresLabel"></span>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="klantVertegenwoordigerLabel">Vertegenwoordiger:</label>
                <span id="klantVertegenwoordigerLabel"></span>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="klantOpmerkingLabel">Korte opmerking:</label>
                <span id="klantOpmerkingLabel"></span>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="klantNotitieDiv">Notitie:</label>
                <div style="margin-left: 150px;" id="klantNotitieDiv"></div>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="klantGroepLabel">Groep:</label>
                <span id="klantGroepLabel"></span>
            </fieldset>

        </div>
    </div>
    <div class="detailsDiv">
        <h3>Contacten</h3>
        <div class="stap heigth280">
            <table>
                <thead>
                    <tr>
                        <th>Naam</th>
                        <th>Functie</th>
                        <th>Telefoon werk</th>
                        <th>Telefoon thuis</th>
                        <th>Mobiel</th>
                        <th>Email</th>
                        <th>Fax</th>
                    </tr>
                </thead>
                <tbody id="contactentBody">
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearleft"></div>
    <div id="ticketdiv">
        <h3>Tickets</h3>
        <div class="stap">
            <table id="ticketTable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Korte omschrijving</th>
                        <th>Klant</th>
                        <th>Contact</th>
                        <th>Toegewezen aan</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>Opvolgingsdatum</th>
                        <th>Garantie</th>
                        <th>Kost DNV</th>
                    </tr>
                </thead>
                <tbody id="ticketTBody">
                </tbody>
            </table>
        </div>
    </div>
    <div class="popup Hidden" id="nieuweKlantPopUp">
        <h4>Nieuwe klant</h4>
        <div class="scrollable">
            <fieldset>
                <label class="labelFor" for="nieuweKlantNummerTxt">Nummer:</label>
                <input type="text" id="nieuweKlantNummerTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantNaamTxt">Naam:</label>
                <input type="text" id="nieuweKlantNaamTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantStraatTxt">Straat:</label>
                <input type="text" id="nieuweKlantStraatTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantPostcodeTxt">Postcode:</label>
                <input type="text" id="nieuweKlantPostcodeTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantGemeenteTxt">Gemeente:</label>
                <input type="text" id="nieuweKlantGemeenteTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantLandTxt">Land:</label>
                <input type="text" id="nieuweKlantLandTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantTelefoonTxt">Telefoon:</label>
                <input type="text" id="nieuweKlantTelefoonTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantNaamVertegenwoordigerTxt">Naam vertegenwoordiger:</label>
                <input type="text" id="nieuweKlantNaamVertegenwoordigerTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantKorteOpmerkingTArea">Korte opmerking:</label>
                <textarea id="nieuweKlantKorteOpmerkingTArea"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantNotitiesTArea">Notities:</label>
                <textarea id="nieuweKlantNotitiesTArea"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="NieuweKlantGroepSelect">Groep:</label>
                <select id="NieuweKlantGroepSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuweKlantHoofdzetelCheckBox">Hoofdzetel:</label>
                <input type="checkbox" id="nieuweKlantHoofdzetelCheckBox" />
            </fieldset>
            <fieldset>
                <button id="nieuweKlantSaveButton" onclick="return KlantOpslaanButton_Click();">Opslaan</button>
                <button id="nieuweKlantcancelButton" onclick="return cancelButton_Click(this);">Annuleren</button>
            </fieldset>
        </div>
    </div>
    <div class="popup Hidden" id="nieuwContactPopUp">
        <h4>Nieuw contact</h4>
        <div class="scrollable">
            <fieldset>
                <label class="labelFor" for="nieuwContactAchternaamTxt">Achternaam:</label>
                <input type="text" id="nieuwContactAchternaamTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactVoornaamTxt">Voornaam:</label>
                <input type="text" id="nieuwContactVoornaamTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactKlantSelect">Klant:</label>
                <select id="nieuwContactKlantSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactFunctieTxt">Functie:</label>
                <input type="text" id="nieuwContactFunctieTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactEmailTArea">Emailadres</label>
                <textarea id="nieuwContactEmailTArea"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactMobielTxt">Mobiel nummer:</label>
                <input type="text" id="nieuwContactMobielTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactTelefoonWerkTxt">Telefoon werk:</label>
                <input type="text" id="nieuwContactTelefoonWerkTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactTelefoonThuisTxt">Telefoon thuis:</label>
                <input type="text" id="nieuwContactTelefoonThuisTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactFaxTxt">fax nummer:</label>
                <input type="text" id="nieuwContactFaxTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactAdresTArea">Adres:</label>
                <textarea id="nieuwContactAdresTArea"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactPlaatTxt">Plaats:</label>
                <input type="text" id="nieuwContactPlaatTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactPostcodeTxt">Postcode:</label>
                <input type="text" id="nieuwContactPostcodeTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactProvincieTxt">Provincie:</label>
                <input type="text" id="nieuwContactProvincieTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactlandTxt">Land:</label>
                <input type="text" id="nieuwContactlandTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactWebpaginaTxt">Webpagina:</label>
                <input type="text" id="nieuwContactWebpaginaTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactKorteOpmerkingTArea">korte opmerking:</label>
                <textarea id="nieuwContactKorteOpmerkingTArea"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwContactNotitiesTArea">Notities:</label>
                <textarea id="nieuwContactNotitiesTArea"></textarea>
            </fieldset>
            <fieldset>
                <button id="nieuwContactsavebutton" onclick="return NieuwContactSavebutton_Click();">Opslaan</button>
                <button id="nieuwContactCancelButton" onclick="return cancelButton_Click(this);">Annuleren</button>
            </fieldset>
        </div>
    </div>
    <div class="popup Hidden" id="nieuweTicketPopUp">
        <h4>Nieuw ticket</h4>
        <div class="scrollable">
            <fieldset>
                <label class="labelFor" for="nieuwTicketOmschrijvingTxt">Korte omschrijving *:</label>
                <input type="text" id="nieuwTicketOmschrijvingTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketKlantSelect">Klant *:</label>
                <select id="nieuwTicketKlantSelect" onchange="return nieuwTicketKlantSelect_Change();"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketContactSelect">Contact:</label>
                <select id="nieuwTicketContactSelect">
                    <option value="5">Dummy Dummy</option>
                </select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketUitgevoerdetakenTArea">Uitgevoerde taken:</label>
                <textarea id="nieuwTicketUitgevoerdetakenTArea"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketToegewezenaanSelect">Toegewezen aan *:</label>
                <select id="nieuwTicketToegewezenaanSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketTypeSelect">Type *:</label>
                <select id="nieuwTicketTypeSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketStatusSelect">Status *:</label>
                <select id="nieuwTicketStatusSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketopvolgingsdatumTxt">Opvolgingsdatum:</label>
                <input type="text" id="nieuwTicketopvolgingsdatumTxt" class="datepicker" />
            </fieldset>
            <div class="panel">
                <fieldset>
                    <label class="labelFor" for="nieuwTicketOrdernummerTxt">Ordernummer:</label>
                    <input type="text" id="nieuwTicketOrdernummerTxt" />
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="nieuwTicketFactuurnummerCP">Factuurnummer C&P:</label>
                    <input type="text" id="nieuwTicketFactuurnummerCP" />
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="nieuwTicketAflevernotaCheckBox">Aflevernota:</label>
                    <input type="text" id="nieuwTicketAflevernotaCheckBox" />
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="nieuwTicketAfleverdatumTxt">Afleverdatum:</label>
                    <input type="text" id="nieuwTicketAfleverdatumTxt" class="datepicker" />
                </fieldset>
            </div>
            <div class="panel">
                <fieldset>
                    <label class="labelFor" for="nieuwTicketLeverancierTxt">Leverancier:</label>
                    <input type="text" id="nieuwTicketLeverancierTxt" />
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="nieuwTicketAankoopbonnummerTxt">Aankoopbonnummer:</label>
                    <input type="text" id="nieuwTicketAankoopbonnummerTxt" />
                </fieldset>
                <fieldset>
                    <label class="labelFor" for="nieuwTicketFacuurnummerTxt">Factuurnummer:</label>
                    <input type="text" id="nieuwTicketFacuurnummerTxt" />
                </fieldset>
            </div>
            <fieldset>
                <label class="labelFor" for="nieuwTicketGarantieSelect">Garantie:</label>
                <select id="nieuwTicketGarantieSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="nieuwTicketKostTxt">Kost DNV:</label>
                <input type="text" id="nieuwTicketKostTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bijlageDiv"></label>
                <div>
                    <!--File Upload -->
                    <b>Bijlagen</b>
                    <br />
                    <div id="attachmentList"></div>
                    <div id="fileList"></div>
                    <div id="addFileList"></div>
                    <div id="fileInput"></div>
                </div>
            </fieldset>
            <fieldset>
                <button id="nieuwTicketSaveButton" onclick="return nieuwTicketSaveButton_Click();">Opslaan</button>
                <button id="nieuwTicketCancelButton" onclick="return cancelButton_Click(this);">Annuleren</button>
            </fieldset>
        </div>
    </div>
    <div class="popup Hidden" id="bewerkTicketPopUp">
        <h4>Bewerk ticket</h4>
        <div class="scrollable">
            <input type="hidden" id="ticketIdHidden" />
            <fieldset>
                <label class="labelFor" for="bewerkTicketOmschrijvingTxt">Korte omschrijving:</label>
                <input type="text" id="bewerkTicketOmschrijvingTxt" />
            </fieldset>
            <%--            <fieldset>
                <label class="labelFor" for="bewerkTicketKlantSelect">Klant:</label>
                <select id="bewerkTicketKlantSelect" onchange="return bewerkTicketKlant_OnChange();"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketContactSelect">Contact:</label>
                <select id="bewerkTicketContactSelect"></select>
            </fieldset>--%>
            <fieldset>
                <label class="labelFor" for="bewerkTicketOrdernummerTxt">Ordernummer:</label>
                <input type="text" id="bewerkTicketOrdernummerTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketFactuurnummerCP">Factuurnummer C&P:</label>
                <input type="text" id="bewerkTicketFactuurnummerCP" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketAflevernotaCheckBox">Aflevernota:</label>
                <input type="checkbox" id="bewerkTicketAflevernotaCheckBox" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketAfleverdatumTxt">Afleverdatum:</label>
                <input type="text" id="bewerkTicketAfleverdatumTxt" class="datepicker" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketAankoopbonnummerTxt">Aankoopbonnummer:</label>
                <input type="text" id="bewerkTicketAankoopbonnummerTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketFacuurnummerTxt">Factuurnummer:</label>
                <input type="text" id="bewerkTicketFacuurnummerTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketToegewezenaanSelect">Toegewezen aan:</label>
                <select id="bewerkTicketToegewezenaanSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketTypeSelect">Type:</label>
                <select id="bewerkTicketTypeSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketStatusSelect">Status:</label>
                <select id="bewerkTicketStatusSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketopvolgingsdatumTxt">Opvolgingsdatum:</label>
                <input type="text" id="bewerkTicketopvolgingsdatumTxt" class="datepicker" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketUitgevoerdetakenTArea">Uitgevoerde taken:</label>
                <textarea id="bewerkTicketUitgevoerdetakenTArea"></textarea>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketGarantieSelect">Garantie:</label>
                <select id="bewerkTicketGarantieSelect"></select>
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketKostTxt">Kost DNV:</label>
                <input type="text" id="bewerkTicketKostTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bewerkTicketLeverancierTxt">Leverancier:</label>
                <input type="text" id="bewerkTicketLeverancierTxt" />
            </fieldset>
            <fieldset>
                <label class="labelFor" for="bijlageDiv"></label>
                <div>
                    <!--File Upload -->
                    <b>Bijlagen</b>
                    <br />
                </div>
            </fieldset>

            <fieldset>
                <button id="bewerkTicketSaveButton" onclick="return bewerkTicketSaveButton_Click();">Opslaan</button>
                <button id="bewerkTicketCancelButton" onclick="return cancelButton_Click(this);">Annuleren</button>
            </fieldset>
        </div>
    </div>
</asp:Content>
