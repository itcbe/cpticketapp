﻿'use strict';
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.exception = be.itc.sp.exception || {};

var ns = be.itc.sp.exception;

// Display error messages. 
ns.onError = function (error, message) {
    var errormessage = message;
    errormessage += '\n\n';
    try {
        errormessage += 'nummer: ' + JSON.parse(error.body).error.code;
        errormessage += '\n\n';
        errormessage += 'melding: ' + JSON.parse(error.body).error.message.value;
    } catch (e) {
        try {
            errormessage += 'nummer: ' + JSON.parse(error.responseText).error.code;
            errormessage += '\n\n';
            errormessage += 'melding: ' + JSON.parse(error.responseText).error.message.value;
        } catch (e) {
            errormessage += error.body;
        }
    }
    alert(errormessage);
};