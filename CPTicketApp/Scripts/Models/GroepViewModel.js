﻿function GetGroepen() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Groepen');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"  
                    + "<Query>" 
                    + "<OrderBy><FieldRef Name='Title' /></OrderBy>" 
                    + "</Query>" 
                    + "<ViewFields>"
                    + "<FieldRef Name='ID' />"
                    + "<FieldRef Name='Title' />"
                    + "</ViewFields>"
                    + " </View>");  

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Naam: item.get_item('Title')
            });
        }
        BindGroepen(arraylist);
        GetTicketStatussen();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de groepen niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindGroepen(arraylist) {
    $("#NieuweKlantGroepSelect").empty();
    if (arraylist.length > 0) {
        $("#NieuweKlantGroepSelect").append('<option></option>');
        for (var i = 0; i < arraylist.length; i++) {
            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].Naam + "</option>";
            $("#NieuweKlantGroepSelect").append(option);
        }
    }
}
