﻿function GetTicketType() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tickets');


        this.fields = list.get_fields();
        this.opdrachten = clientContext.castTo(list.get_fields().getByInternalNameOrTitle("Type_x0020_ticket"), SP.FieldChoice);

        clientContext.load(fields);
        clientContext.load(opdrachten);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var context = new SP.ClientContext.get_current();
        // Converting the Field to SPFieldChoice from the execution results
        var myChoicesfield = context.castTo(this.fields.getByInternalNameOrTitle("Type_x0020_ticket"), SP.FieldChoice);
        //get_choices() method will return the array of choices provided in the field

        var choices = myChoicesfield.get_choices();

        var TypeSelect = $("#nieuwTicketTypeSelect");
        var bewerkTypeSelect = $('#bewerkTicketTypeSelect');
        TypeSelect.empty();
        bewerkTypeSelect.empty();
        if (choices.length > 0) {
            bewerkTypeSelect.append("<option></option>");
            TypeSelect.append("<option></option>");
            for (var i = 0; i < choices.length; i++) {
                bewerkTypeSelect.append("<option value='" + choices[i] + "'>" + choices[i] + "</option>");
                TypeSelect.append("<option value='" + choices[i] + "'>" + choices[i] + "</option>");
            }
        }
        GetUsers();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de type niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}