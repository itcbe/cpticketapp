﻿function GetKlanten() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"  
                    + "<Query> "
                    + "<OrderBy><FieldRef Name='Volledige_x0020_naam' /></OrderBy>" 
                    + "</Query>" 
                    + "<ViewFields>"
                    + "<FieldRef Name='ID' />"
                    + "<FieldRef Name='Naam1' />"
                    + "<FieldRef Name='Gemeente' />"
                    + "<FieldRef Name='Hoofdzetel' />"
                    + "<FieldRef Name='Title' />"
                    + "</ViewFields>" 
                    + "</View>");  

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Naam: item.get_item('Naam1'),
                Gemeente: item.get_item('Gemeente'),
                Hoofdzetel: item.get_item('Hoofdzetel'),
                Nummer: item.get_item('Title')
            });
        }
        BindKlanten(arraylist);
        GetGroepen();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlanten(arraylist) {
    $('#klantselect').empty();
    $('#nieuwContactKlantSelect').empty();
    $('#nieuwTicketKlantSelect').empty();
    $('#bewerkTicketKlantSelect').empty();
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            var volledigeNaam = arraylist[i].Nummer + " - " + arraylist[i].Naam + " - " + arraylist[i].Gemeente + ((arraylist[i].Hoofdzetel == true ? " - Hoofdzetel" : ""));
            var option = "<option value='" + arraylist[i].ID + "'>" + volledigeNaam + "</option>";
            $('#klantselect').append(option);
            $('#nieuwContactKlantSelect').append(option);
            $('#nieuwTicketKlantSelect').append(option);
            $('#bewerkTicketKlantSelect').append(option);
        }
    }
    $("#klantselect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
    $("#nieuwContactKlantSelect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
    $("#nieuwTicketKlantSelect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
    $("#bewerkTicketKlantSelect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
    $("#zoekResultaatLabel").text((arraylist.length) + " klanten gevonden.");
    CacheItems();
}

/************************ Get customer by id ****************************/

function GetCustomerById(klantid, mode) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                    + "<Query>"
                    + "<Where>"
                    + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + klantid + "</Value></Eq>"
                    + "</Where>"
                    + "</Query>"
                    + "<ViewFields>"
                    + "<FieldRef Name='ID' />"
                    + "<FieldRef Name='Naam1' />"
                    + "<FieldRef Name='Gemeente' />"
                    + "<FieldRef Name='Hoofdzetel' />"
                    + "<FieldRef Name='Korte_x0020_opmerking' />"
                    + "<FieldRef Name='Land' />"
                    + "<FieldRef Name='Notities' />"
                    + "<FieldRef Name='Title' />"
                    + "<FieldRef Name='Postnr' />"
                    + "<FieldRef Name='Straat' />"
                    + "<FieldRef Name='Telefoon' />"
                    + "<FieldRef Name='Volledige_x0020_naam' />"
                    + "<FieldRef Name='Naam_x0020_vertegenwoordiger' />"
                    + "<FieldRef Name='Groep' />"
                    + "</ViewFields>"
                    + "<RowLimit>1</RowLimit>"
                    + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var volledigeNaam = item.get_item('Title') + " - " + item.get_item('Naam1') + " - " + item.get_item('Gemeente') + ((item.get_item('Hoofdzetel') == true ? " - Hoofdzetel" : ""));
            $('#klantNaamLabel').text(volledigeNaam);
            $('#klantTelefoonLabel').text(item.get_item('Telefoon') == null ? "" : item.get_item('Telefoon'));
            $('#klantAdresLabel').html(item.get_item('Straat') + " " + item.get_item('Postnr') + " " + item.get_item('Gemeente') + " " + item.get_item('Land'));
            $('#klantVertegenwoordigerLabel').text(item.get_item('Naam_x0020_vertegenwoordiger') == null ? "" : item.get_item('Naam_x0020_vertegenwoordiger'));
            $('#klantOpmerkingLabel').text(item.get_item('Korte_x0020_opmerking') == null ? "" : item.get_item('Korte_x0020_opmerking'));
            $('#klantNotitieDiv').html(item.get_item('Notities') == null ? "" : item.get_item('Notities'));
            $('#klantGroepLabel').text(item.get_item('Groep') == null ? "" : item.get_item('Groep').get_lookupValue());
            if ($('#bewerkKlantButton').hasClass('Hidden'))
                $('#bewerkKlantButton').removeClass('Hidden');
        }
        //if (mode == 'bewerk')
        //    be.itc.sp.rest.fileupload.showAttachments($('#ticketIdHidden').val());
        //else
        GetContactenForTicket(klantid, mode);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

/************************ nieuwe klant opslaan *****************************/
function KlantOpslaanButton_Click() {
    var klant = GetNieuweKlantFromForm();
    if (IsKlantValid(klant)) {
        SaveKlant(klant);
    }
    return false;
}

function GetNieuweKlantFromForm() {
    var nummer = $('#nieuweKlantNummerTxt').val();
    var naam = $('#nieuweKlantNaamTxt').val();
    var straat = $('#nieuweKlantStraatTxt').val();
    var postcode = $('#nieuweKlantPostcodeTxt').val();
    var gemeente = $('#nieuweKlantGemeenteTxt').val();
    var land = $('#nieuweKlantLandTxt').val();
    var telefoon = $('#nieuweKlantTelefoonTxt').val();
    var vertegenwoordiger = $('#nieuweKlantNaamVertegenwoordigerTxt').val();
    var opmerking = $('#nieuweKlantKorteOpmerkingTArea').val();
    var notitie = $('#nieuweKlantNotitiesTArea').val();
    var groep = $('#NieuweKlantGroepSelect').val();
    var hoofdzetel = $('#nieuweKlantHoofdzetelCheckBox').is(':checked') == true ? 1 : 0;

    klant = {
        Nummer: nummer,
        Naam: naam,
        Straat: straat,
        Postcode: postcode,
        Gemeente: gemeente,
        Land: land,
        Telefoon: telefoon,
        Vertegenwoordiger: vertegenwoordiger,
        Opmerking: opmerking,
        Notitie: notitie,
        Groep: groep,
        Hoofdzetel: hoofdzetel
    }

    return klant;
}

function IsKlantValid(klant) {
    var valid = true;
    var errortext = "";
    if (klant.Nummer == "") {
        valid = false;
        errortext += "Klant nummer is niet ingevuld!<br />";
    }
    if (klant.Naam == "") {
        valid = false;
        errortext += "Klant naam is niet ingevuld!<br />";
    }

    if (!valid) {
        alert(errortext);
    }
    return valid;
}

function SaveKlant(klant) {
    var self = this;
    self.SaveKlant = function () {
        if (klant) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);

            item.set_item("Title", klant.Nummer);
            //item.set_item("Naam", klant.Naam);
            item.set_item("Naam1", klant.Naam);
            item.set_item("Straat", klant.Straat);
            item.set_item("Gemeente", klant.Gemeente)
            item.set_item("Postnr", klant.Postcode);
            item.set_item("Land", klant.Land);
            item.set_item("Telefoon", klant.Telefoon);
            item.set_item("Naam_x0020_vertegenwoordiger", klant.Vertegenwoordiger);
            item.set_item("Korte_x0020_opmerking", klant.Opmerking);
            item.set_item("Notities", klant.Notitie);
            item.set_item("Groep", klant.Groep);
            item.set_item("Hoofdzetel", klant.Hoofdzetel);

            item.update();
            context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
        }
    }

    self.onQuerySucceeded = function (sender, args) {
        alert("Klant opgeslagen.");
        GetKlanten();
        $("#nieuweKlantPopUp").addClass('Hidden');
        $('.overlay').addClass('Hidden');
        ClearKlantForm();
    }

    self.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van de Klant: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.SaveKlant();
}

/********************** Andere operaties **************************/

function nieuwTicketKlantSelect_Change() {
    var klantid = $('#nieuwTicketKlantSelect').val();
    GetContactenForTicket(klantid, "nieuw");
}

function bewerkTicketKlant_OnChange() {
    var klantid = $('#bewerkTicketKlantSelect').val();
    GetContactenForTicket(klantid, "bewerk");
}

function klantSelect_Change() {
    var klantid = $("#klantselect").val();
    if (klantid == 0 || klantid == "0")
        ClearKlant();
    else
        GetTickets(klantid);
}

function ClearKlantForm() {
    $('#nieuweKlantNummerTxt').val("");
    $('#nieuweKlantNaamTxt').val("");
    $('#nieuweKlantStraatTxt').val("");
    $('#nieuweKlantPostcodeTxt').val("");
    $('#nieuweKlantGemeenteTxt').val("");
    $('#nieuweKlantLandTxt').val("");
    $('#nieuweKlantTelefoonTxt').val("");
    $('#nieuweKlantNaamVertegenwoordigerTxt').val("");
    $('#nieuweKlantKorteOpmerkingTArea').val("");
    $('#nieuweKlantNotitiesTArea').val("");
    $('#NieuweKlantGroepSelect').val([]);
    $('#nieuweKlantHoofdzetelCheckBox').is(':checked', false);
}

function ClearKlant() {
    $('#klantNaamLabel').text("");
    $('#klantTelefoonLabel').text("");
    $('#klantAdresLabel').html("");
    $('#klantVertegenwoordigerLabel').text("");
    $('#klantOpmerkingLabel').text("");
    $('#klantNotitieDiv').html("");
    $('#klantGroepLabel').text("");
    if (!$('#bewerkKlantButton').hasClass('Hidden'))
        $('#bewerkKlantButton').addClass('Hidden');
    $("#contactentBody").empty();
}
