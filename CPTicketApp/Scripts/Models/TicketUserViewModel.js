﻿function GetUsers() {
    var self = this;
    var collGroup = null;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        collGroup = clientContext.get_web().get_siteGroups();
        var oGroup = collGroup.getByName('CPfurnitures')
        this.collUser = oGroup.get_users();
        clientContext.load(collUser);
        //clientContext.load(collGroup);
        //clientContext.load(collGroup, 'Include(Users)');

        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );

    }

    self._onLoadListSucceeded = function (sender, args) {
        var userEnumerator = this.collUser.getEnumerator();
        var arraylist = [];

        while (userEnumerator.moveNext()) {
            var oUser = userEnumerator.get_current();

            arraylist.push(
            {
                ID: oUser.get_id(),
                Titel: oUser.get_title(),
                LoginName: oUser.get_loginName(),
                Email: oUser.get_email()
            });
            //var oGroup = groupEnumerator.get_current();
            //var collUser = oGroup.get_users();
            //var userEnumerator = collUser.getEnumerator();

            //while (userEnumerator.moveNext()) {
            //    var oUser = userEnumerator.get_current();

            //    arraylist.push(
            //    {
            //        ID: oUser.get_id(),
            //        Titel: oUser.get_title(),
            //        LoginName: oUser.get_loginName(),
            //        Email: oUser.get_email()
            //    });
            //}
        }
        BindTechniekers(arraylist);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de gebruikers niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindTechniekers(arraylist) {
    $("#nieuwTicketToegewezenaanSelect").empty();
    //$("#bewerkTicketToegewezenaanSelect").empty();
    for (var i = 0; i < arraylist.length; i++) {
        if (arraylist[i].Titel !== "Administrator" && arraylist[i].Titel !== "Systeemaccount") {
            var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].Titel + "</option>";
            $("#nieuwTicketToegewezenaanSelect").append(option);
            //$("#bewerkTicketToegewezenaanSelect").append(option);
        }
    }
    $("#nieuwTicketToegewezenaanSelect").prepend('<option></option>');
}