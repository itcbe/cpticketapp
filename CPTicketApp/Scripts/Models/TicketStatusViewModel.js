﻿function GetTicketStatussen() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tickets');


        this.fields = list.get_fields();
        this.opdrachten = clientContext.castTo(list.get_fields().getByInternalNameOrTitle("Status"), SP.FieldChoice);

        clientContext.load(fields);
        clientContext.load(opdrachten);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var context = new SP.ClientContext.get_current();
        // Converting the Field to SPFieldChoice from the execution results
        var myChoicesfield = context.castTo(this.fields.getByInternalNameOrTitle("Status"), SP.FieldChoice);
        //get_choices() method will return the array of choices provided in the field

        var choices = myChoicesfield.get_choices();

        var StatusSelect = $("#nieuwTicketStatusSelect");
        var bewerkStatusSelect = $('#bewerkTicketStatusSelect');
        StatusSelect.empty();
        if (choices.length > 0) {
            
            for (var i = 0; i < choices.length; i++) {
                bewerkStatusSelect.append("<option value='" + choices[i] + "'>" + choices[i] + "</option>");
                StatusSelect.append("<option value='" + choices[i] + "'>" + choices[i] + "</option>");
            }
        }
        //StatusSelect.empty();
        //if (choices.length > 0) {
        //    for (var i = 0; i < choices.length; i++) {
        //        StatusSelect.append("<option value='" + choices[i] + "'>" + choices[i] + "</option>");
        //    }
        //}
        GetTicketGaranties();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de statussen niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}
