﻿function GetContactenForTicket(klantid, element) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                    + "<Query>"
                    + "<Where>"
                    + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                    + "</Where>"
                    + "<OrderBy><FieldRef Name='Volledige_x0020_naam' /></OrderBy>"
                    + "</Query>"
                    + "<ViewFields>"
                    + "<FieldRef Name='Title' />"
                    + "<FieldRef Name='Adres' />"
                    + "<FieldRef Name='E_x002d_mailadres' />"
                    + "<FieldRef Name='Fax_x0020_nummer' />"
                    + "<FieldRef Name='Functie' />"
                    + "<FieldRef Name='ID' />"
                    + "<FieldRef Name='Klant' />"
                    + "<FieldRef Name='Korte_x0020_opmerking' />"
                    + "<FieldRef Name='Land' />"
                    + "<FieldRef Name='Mobiel_x0020_nummer' />"
                    + "<FieldRef Name='Notities' />"
                    + "<FieldRef Name='Plaats' />"
                    + "<FieldRef Name='Postcode' />"
                    + "<FieldRef Name='Provincie' />"
                    + "<FieldRef Name='Telefoon_x0020_thuis' />"
                    + "<FieldRef Name='Telefoon_x0020_werk' />"
                    + "<FieldRef Name='Voornaam' />"
                    + "<FieldRef Name='Webpagina' />"
                    + "<FieldRef Name='Volledige_x0020_naam' />"
                    + "</ViewFields>"
                    + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                ID: item.get_item('ID'),
                Adres: item.get_item('Adres'),
                Achternaam: item.get_item('Title'),
                Email: item.get_item('E_x002d_mailadres'),
                Fax: item.get_item('Fax_x0020_nummer') == null ? "" : item.get_item('Fax_x0020_nummer'),
                Functie: item.get_item('Functie'),
                Klant: item.get_item('Klant'),
                Opmerking: item.get_item('Korte_x0020_opmerking'),
                Land: item.get_item('Land'),
                Mobielnummer: item.get_item('Mobiel_x0020_nummer') == null ? "" : item.get_item('Mobiel_x0020_nummer'),
                Notities: item.get_item('Notities'),
                Plaats: item.get_item('Plaats'),
                Postcode: item.get_item('Postcode'),
                Provincie: item.get_item('Provincie'),
                TelefoonThuis: item.get_item('Telefoon_x0020_thuis') == null ? "" : item.get_item('Telefoon_x0020_thuis'),
                TelefoonWerk: item.get_item('Telefoon_x0020_werk') == null ? "" : item.get_item('Telefoon_x0020_werk'),
                Voornaam: item.get_item('Voornaam'),
                Webpagina: item.get_item('Webpagina'),
                VolledigeNaam: item.get_item('Volledige_x0020_naam')
            });
        }
        BindContacten(arraylist, element);
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindContacten(arraylist, element) {
    if (element == "nieuw") {
        $("#nieuwTicketContactSelect").empty();
        $('#nieuwTicketContactSelect').append("<option value='5'>Dummy Dummy</option>");
        if (arraylist.length > 0) {
            for (var i = 0; i < arraylist.length; i++) {
                var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].VolledigeNaam + "</option>";
                $("#nieuwTicketContactSelect").append(option);
            }
        }
    } else if (element === "bewerk") {
        $("#bewerkTicketContactSelect").empty();
        if (arraylist.length > 0) {
            for (var i = 0; i < arraylist.length; i++) {
                var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].VolledigeNaam + "</option>";
                $("#bewerkTicketContactSelect").append(option);
            }
        }
    }
    else {
        $("#contactentBody").empty();
        if (arraylist.length > 0) {
            for (var i = 0; i < arraylist.length; i++) {
                var tablerow = "<tr>";
                tablerow += "<td>" + arraylist[i].VolledigeNaam + "</td>";
                tablerow += "<td>" + arraylist[i].Functie + "</td>";
                tablerow += "<td>" + arraylist[i].TelefoonWerk + "</td>";
                tablerow += "<td>" + arraylist[i].TelefoonThuis + "</td>";
                tablerow += "<td>" + arraylist[i].Mobielnummer + "</td>";
                tablerow += "<td>" + arraylist[i].Email + "</td>";
                tablerow += "<td>" + arraylist[i].Fax + "</td>";
                tablerow += "<td><a onclick='bewerkContact(" + arraylist[i].ID + ");' class='Buttons'>Bewerk</a></td>"
                tablerow += "</tr>";

                $("#contactentBody").append(tablerow);
            }
        }
    }
}

function bewerkContact(id) {
    var url = "https://cpfurnitures.sharepoint.com/Lists/Contactpersonen/EditForm.aspx?ID=" + id;
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

/****************************  nieuw contact opslaan **********************/

function NieuwContactSavebutton_Click() {
    var contact = GetcontactFromForm();
    if (IsContactValid(contact)) {
        SaveContact(contact);
    }
    return false;
}

function GetcontactFromForm() {
    var achternaam = $('#nieuwContactAchternaamTxt').val();
    var voornaam = $('#nieuwContactVoornaamTxt').val();
    var klantid = $('#nieuwContactKlantSelect').val();
    var functie = $('#nieuwContactFunctieTxt').val();
    var email = $('#nieuwContactEmailTArea').val();
    var mobielnummer = $('#nieuwContactMobielTxt').val();
    var telefoonwerk = $('#nieuwContactTelefoonWerkTxt').val();
    var telefoonthuis = $('#nieuwContactTelefoonThuisTxt').val();
    var fax = $('#nieuwContactFaxTxt').val();
    var adres = $('#nieuwContactAdresTArea').val();
    var plaats = $('#nieuwContactPlaatTxt').val();
    var postcode = $('#nieuwContactPostcodeTxt').val();
    var provincie = $('#nieuwContactProvincieTxt').val();
    var land = $('#nieuwContactlandTxt').val();
    var webpagina = $('#nieuwContactWebpaginaTxt').val();
    var opmerking = $('#nieuwContactKorteOpmerkingTArea').val();
    var notitie = $('#nieuwContactNotitiesTArea').val();

    contact = {
        Achternaam: achternaam,
        Voornaam: voornaam,
        Klantid: klantid,
        Functie: functie,
        Email: email,
        Mobielnummer: mobielnummer,
        TelefoonWerk: telefoonwerk,
        TelefoonThuis: telefoonthuis,
        Faxnummer: fax,
        Adres: adres,
        Plaats: plaats,
        Postcode: postcode,
        Provincie: provincie,
        Land: land,
        Webpagina: webpagina,
        Opmerking: opmerking,
        Notitie: notitie
    };
    return contact;
}

function IsContactValid(contact) {
    var valid = true;
    var errortext = "";
    if (contact.Achternaam === "") {
        valid = false;
        errortext += "Achternaam is niet ingevuld!\n";
    }
    if (contact.Voornaam === "") {
        var valid = false;
        errortext += "Voornaam is niet ingevuld!\n";
    }

    if (!valid) {
        alert(errortext);
    }

    return valid;
}

function SaveContact(contact) {
    var self = this;
    self.SaveContact = function () {
        if (contact) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Contactpersonen');

            var itemCreateInfo = new SP.ListItemCreationInformation();
            var item = list.addItem(itemCreateInfo);

            item.set_item("Title", contact.Achternaam);
            item.set_item("Voornaam", contact.Voornaam);
            item.set_item("Klant", contact.Klantid);
            item.set_item("Functie", contact.Functie);
            item.set_item("E_x002d_mailadres", contact.Email);
            item.set_item("Adres", contact.Adres);
            item.set_item("Plaats", contact.Plaats)
            item.set_item("Postcode", contact.Postcode);
            item.set_item("Provincie", contact.Provincie);
            item.set_item("Land", contact.Land);
            item.set_item("Telefoon_x0020_werk", contact.TelefoonWerk);
            item.set_item("Telefoon_x0020_thuis", contact.TelefoonThuis);
            item.set_item("Fax_x0020_nummer", contact.Faxnummer);
            item.set_item("Korte_x0020_opmerking", contact.Opmerking);
            item.set_item('Mobiel_x0020_nummer', contact.Mobielnummer);
            item.set_item('Notities', contact.Notitie);
            item.set_item('Webpagina', contact.Webpagina);
            item.update();
            context.executeQueryAsync(onQuerySucceeded, onQueryFailed);
        }
    }

    self.onQuerySucceeded = function (sender, args) {
        alert("Contact opgeslagen.");
        $("#nieuwContactPopUp").addClass('Hidden');
        $('.overlay').addClass('Hidden');
        ClearContactForm();
    }

    self.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van het contact: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.SaveContact();
}

function ClearContactForm() {
    $('#nieuwContactAchternaamTxt').val("");
    $('#nieuwContactVoornaamTxt').val("");
    //$('#nieuwContactKlantSelect').val("");
    $('#nieuwContactFunctieTxt').val("");
    $('#nieuwContactEmailTArea').val("");
    $('#nieuwContactMobielTxt').val("");
    $('#nieuwContactTelefoonWerkTxt').val("");
    $('#nieuwContactTelefoonThuisTxt').val("");
    $('#nieuwContactFaxTxt').val("");
    $('#nieuwContactAdresTArea').val("");
    $('#nieuwContactPlaatTxt').val("");
    $('#nieuwContactPostcodeTxt').val("");
    $('#nieuwContactProvincieTxt').val("");
    $('#nieuwContactlandTxt').val("");
    $('#nieuwContactWebpaginaTxt').val("");
    $('#nieuwContactKorteOpmerkingTArea').val("");
    $('#nieuwContactNotitiesTArea').val("");
}
