﻿function GetTickets(klantid) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tickets');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                + "<Query>"
                + "<Where>"
                + "<And>"
                + "<Neq><FieldRef Name='Status' /><Value Type='Choice'>Afgesloten</Value></Neq>"
                + "<Eq><FieldRef Name='Klant' LookupId='True' /><Value Type='Lookup'>" + klantid + "</Value></Eq>"
                + "</And>"
                + "</Where>"
                + "<OrderBy><FieldRef Name='ID' /></OrderBy>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='Aankoopbonnummer' />"
                + "<FieldRef Name='Afleverdatum' />"
                + "<FieldRef Name='Aflevernota' />"
                + "<FieldRef Name='Contactpersoon' />"
                + "<FieldRef Name='Factuurnummer' />"
                + "<FieldRef Name='Factuurnummer_x0020_C_x0026_P' />"
                + "<FieldRef Name='Garantie' />"
                + "<FieldRef Name='Modified' />"
                + "<FieldRef Name='Editor' />"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Klant' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Kost_x0020_DNV' />"
                + "<FieldRef Name='Leverancier' />"
                + "<FieldRef Name='Opvolgingsdatum' />"
                + "<FieldRef Name='Ordernummer' />"
                + "<FieldRef Name='Status' />"
                + "<FieldRef Name='Toegewezen_x0020_aan' />"
                + "<FieldRef Name='Type_x0020_ticket' />"
                + "</ViewFields>"
                + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contactpersoon');
            if (contact != null)
                contact = contact.get_lookupValue();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            if (toegewezen != null) {
                for (var i = 0; i < toegewezen.length; i++) {
                    var userName = toegewezen[i].get_lookupValue();
                    usertext = usertext + userName + '; ';
                }
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, true);
            var afleverdate = item.get_item('Afleverdatum').toString();
            var aflever = ToDateString(afleverdate, false);
            //var bijlagen = item.get_item('Attachments');
            arraylist.push(
            {
                Type: item.get_item('Type_x0020_ticket'),
                ID: item.get_item('ID'),
                Klant: klant,
                KlantId: myklantid,
                Omschrijving: item.get_item('Title'),
                Contact: contact,
                ToegewezenAan: usertext,
                Status: item.get_item('Status'),
                Opvolgdatum: opvolg,
                Aankoopbonnummer: item.get_item('Aankoopbonnummer'),
                Aflevernota: item.get_item('Aflevernota'),
                Factuurnummer: item.get_item('Factuurnummer'),
                FactuurnummerCP: item.get_item('Factuurnummer_x0020_C_x0026_P'),
                Garantie: item.get_item('Garantie'),
                KostDNV: item.get_item('Kost_x0020_DNV'),
                Leverancier: item.get_item('Leverancier'),
                Ordernummer: item.get_item('Ordernummer'),
                afleverdatum: aflever,
                Gewijzigd: gewijzigd,
                GewijzigdDoor: gewijzigddoor
            });
        }
        BindTickets(arraylist);
        var klantid = $('#klantselect').val();
        GetCustomerById(klantid, "toon");
        if (firstTime) {
            $("#ticketTable").tablesorter({
                sortList: [[2, 1]]
            });
            $("#ticketTable").trigger("updateAll");
            firstTime = false;
        }
        else
            $("#ticketTable").trigger("updateAll");
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de tickets niet laden!: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindTickets(arraylist) {
    $('#ticketTBody').empty();
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            var tablerow = "<tr>";
            tablerow += "<td>" + arraylist[i].ID + "</td>";
            tablerow += "<td>" + arraylist[i].Omschrijving + "</td>";
            tablerow += "<td>" + arraylist[i].Klant + "</td>";
            tablerow += "<td>" + arraylist[i].Contact + "</td>";
            tablerow += "<td>" + arraylist[i].ToegewezenAan + "</td>";
            tablerow += "<td>" + arraylist[i].Type + "</td>";
            tablerow += "<td>" + arraylist[i].Status + "</td>";
            tablerow += "<td>" + arraylist[i].Opvolgdatum + "</td>";
            tablerow += "<td>" + arraylist[i].Garantie + "</td>";
            tablerow += "<td>" + (arraylist[i].KostDNV == null ? "" : arraylist[i].KostDNV) + "</td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return OpenTicket(" + arraylist[i].ID + ");' ><img src='../Images/Open.gif' alt='Open ticket' width='23' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return BewerkTicket(" + arraylist[i].ID + ");' ><img src='../Images/Edit.png' alt='Bewerk ticket' width='18' /></a></td>";
            tablerow += "<td style='padding:6px; margin: 3px;'><a href='#' onclick='return PrintTicket(" + arraylist[i].ID + ");' ><img src='../Images/print.gif' alt='Print ticket' width='18' /></a></td>";
            tablerow += "</tr>";
            $('#ticketTBody').append(tablerow);
        }
        $('#ticketTBody tr').hover(function () { $(this).addClass('row-highlight'); }, function () { $(this).removeClass('row-highlight'); });
    }
}

function LoadDetails(id) {
    GetTicketById(id, false);
    return false;
}

/******************** Get ticket by ID **********************/

function GetTicketById(ticketid, bewerk) {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tickets');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                + "<Query>"
                + "<Where>"
                + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + ticketid + "</Value></Eq>"
                + "</Where>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='Aankoopbonnummer' />"
                + "<FieldRef Name='Afleverdatum' />"
                + "<FieldRef Name='Aflevernota' />"
                + "<FieldRef Name='Contactpersoon' />"
                + "<FieldRef Name='Factuurnummer' />"
                + "<FieldRef Name='Factuurnummer_x0020_C_x0026_P' />"
                + "<FieldRef Name='Garantie' />"
                + "<FieldRef Name='Modified' />"
                + "<FieldRef Name='Editor' />"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Klant' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Kost_x0020_DNV' />"
                + "<FieldRef Name='Leverancier' />"
                + "<FieldRef Name='Opvolgingsdatum' />"
                + "<FieldRef Name='Ordernummer' />"
                + "<FieldRef Name='Status' />"
                + "<FieldRef Name='Toegewezen_x0020_aan' />"
                + "<FieldRef Name='Type_x0020_ticket' />"
                + "<FieldRef Name='Uitgevoerde_x0020_taken' />"
                + "</ViewFields>"
                + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contactpersoon').get_lookupValue();
            var contactid = item.get_item('Contactpersoon').get_lookupId();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";
            if (toegewezen != null) {
                for (var i = 0; i < toegewezen.length; i++) {
                    var userName = toegewezen[i].get_lookupValue();
                    usertext = usertext + userName + '; ';
                }
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, true);
            var afleverdate = item.get_item('Afleverdatum').toString();
            var aflever = ToDateString(afleverdate, true);
            var aflevernota = item.get_item('Aflevernota');
            var kost = item.get_item('Kost_x0020_DNV');

            $('#ticketOrdernummerLabel').text(item.get_item('Ordernummer'));
            $('#ticketfactuurCPLabel').text(item.get_item('Factuurnummer_x0020_C_x0026_P'));
            $('#ticketAflevernotaLabel').text(aflevernota);
            $('#ticketAfleverdatumLabel').text(aflever);
            $('#ticketOmschrijvingLabel').text(item.get_item('Title'));
            $('#ticketLeverancierLabel').text(item.get_item('Leverancier'));
            $('#ticketAankoopbonnummerLabel').text(item.get_item('Aankoopbonnummer'));
            $('#ticketFactuurnummerLabel').text(item.get_item('Factuurnummer'));

            if (bewerk) {
                $("#ticketIdHidden").val(item.get_item('ID'));
                $('#bewerkTicketOmschrijvingTxt').val(item.get_item('Title'));
                $('#bewerkTicketOrdernummerTxt').val(item.get_item('Ordernummer'));
                $('#bewerkTicketFactuurnummerCP').val(item.get_item('Factuurnummer_x0020_C_x0026_P'));
                $('#bewerkTicketAflevernotaCheckBox').attr('checked', aflevernota);
                $('#bewerkTicketAfleverdatumTxt').val(aflever);
                $('#bewerkTicketAankoopbonnummerTxt').val(item.get_item('Aankoopbonnummer'));
                $('#bewerkTicketFacuurnummerTxt').val(item.get_item('factuurnummer'));
                $('#bewerkTicketToegewezenaanSelect').val(usertext);

                $('#bewerkTicketTypeSelect').val([]);
                var typeticket = item.get_item('Type_x0020_ticket');
                $('#bewerkTicketTypeSelect option').each(function () {
                    var opt = $(this);
                    var type = opt.text();
                    if (type == typeticket)
                        opt.attr('selected', 'selected');
                });

                $('#bewerkTicketStatusSelect').val([]);
                var status = item.get_item('Status');
                $('#bewerkTicketStatusSelect option').each(function () {
                    var opt = $(this);
                    var thisstatus = opt.text()
                    if (thisstatus == status)
                        opt.attr('selected', 'selected');
                });

                $('#bewerkTicketopvolgingsdatumTxt').val(opvolg);
                $('#bewerkTicketUitgevoerdetakenTArea').html(item.get_item('Uitgevoerde_x0020_taken'));
                $('#bewerkTicketGarantieSelect').val([]);
                var garantie = item.get_item('Garantie');
                $('#bewerkTicketGarantieSelect option').each(function () {
                    var opt = $(this);
                    var thisgarantie = opt.text()
                    if (thisgarantie == garantie)
                        opt.attr('selected', 'selected');
                });

                $('#bewerkTicketKostTxt').val(kost);
                $('#bewerkTicketLeverancierTxt').val(item.get_item('Leverancier'));
                $('#bewerkTicketContactSelect').val(contactid);

                $('#bewerkTicketPopUp').removeClass('Hidden');
                $('.overlay').removeClass('Hidden');
                var klantid = $('#klantselect').val();
                GetCustomerById(klantid, "bewerk");
            }
            else {
                var klantid = $('#klantselect').val();
                GetCustomerById(klantid, "toon");
            }



        }
    }

    self.loadList();
}

/******************** NieuwTicket Opslaan **********************/

function nieuwTicketSaveButton_Click() {
    var ticket = GetNieuwTicketFromForm();
    if (IsTicketValid(ticket)) {
        SaveNieuwTicket(ticket);
    }
    return false;
}

function GetNieuwTicketFromForm() {
    var klantid = $("#nieuwTicketKlantSelect").val();
    var contactid = $("#nieuwTicketContactSelect").val();
    var omschrijving = $("#nieuwTicketOmschrijvingTxt").val();
    var ordernummer = $("#nieuwTicketOrdernummerTxt").val();
    var factuurCP = $("#nieuwTicketFactuurnummerCP").val();
    var aflevernota = $("#nieuwTicketAflevernotaCheckBox").val();
    var afleverdatum = ToSharePointDate($("#nieuwTicketAfleverdatumTxt").val());
    var aankoopbonnummer = $("#nieuwTicketAankoopbonnummerTxt").val();
    var factuurnummer = $("#nieuwTicketFacuurnummerTxt").val();
    var toegewezenaan = [];
    if ($('#nieuwTicketToegewezenaanSelect :selected'))
        toegewezenaan.push({ ID: $('#nieuwTicketToegewezenaanSelect :selected').val(), Naam: $('#nieuwTicketToegewezenaanSelect :selected').text() });
    var typeTicket = $("#nieuwTicketTypeSelect").val();
    var taken = tinyMCE.get('nieuwTicketUitgevoerdetakenTArea').getContent({ format: 'raw' });
    taken = CleanText(taken);
    //var taken = CleanOmschrijving($("#nieuwTicketUitgevoerdetakenTArea").val());
    var opvolgdatum = ToSharePointDate($("#nieuwTicketopvolgingsdatumTxt").val());
    var status = $("#nieuwTicketStatusSelect").val();
    var garantie = $("#nieuwTicketGarantieSelect").val();
    var kost = $("#nieuwTicketKostTxt").val();
    var leverancier = $("#nieuwTicketLeverancierTxt").val();
    ticket = {
        KlantId: klantid,
        ContactId: contactid,
        Omschrijving: omschrijving,
        Ordernummer: ordernummer,
        FactuurCP: factuurCP,
        Aflevernota: aflevernota,
        Afleverdatum: afleverdatum,
        Aankoopbonnummer: aankoopbonnummer,
        Factuurnummer: factuurnummer,
        TypeTicket: typeTicket,
        UitgevoerdeTaken: taken,
        ToegewezenAan: toegewezenaan,
        Opvolgingsdatum: opvolgdatum,
        Status: status,
        Garantie: garantie,
        KostDNV: kost,
        Leverancier: leverancier
    }

    return ticket;
}

function IsTicketValid(ticket) {
    var valid = true;
    var errortext = "";
    if (ticket.KlantId === "") {
        valid = false;
        errortext += "- Geen klant gekozen!\n";
    }
    if (ticket.ContactId === "" || ticket.ContactId === null) {
        valid = false;
        errortext += "- Geen contact gekozen!\n";
    }
    if (ticket.Omschrijving === "") {
        valid = false;
        errortext += "- Geen Korte omschrijving ingegeven!\n";
    }
    if (ticket.TypeTicket === "" || ticket.TypeTicket === null) {
        valid = false;
        errortext += "- Geen type ticket gekozen!\n";
    }
    if (ticket.Status === "" || ticket.Status === null) {
        valid = false;
        errortext += "- Geen status gekozen!\n";
    }
    if (ticket.ToegewezenAan.length < 1) {
        valid = false;
        errortext += "- Geen toegewezen aan gekozen!\n";
    } else if (ticket.ToegewezenAan[0].ID === "" || ticket.ToegewezenAan[0].Naam === "") {
        valid = false;
        errortext += "- Geen toegewezen aan gekozen!\n";
    }

    if (!valid) {
        alert(errortext);
    }
    return valid;
}

var item = null;
function SaveNieuwTicket(ticket) {
    this.save = function () {
        if (ticket) {
            var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
            var list = hostWebContext.get_web().get_lists().getByTitle('Tickets');
            var itemCreateInfo = new SP.ListItemCreationInformation();
            item = list.addItem(itemCreateInfo);
            //context.load(item);
            var klantlookupvalue = new SP.FieldLookupValue();
            klantlookupvalue.set_lookupId(ticket.KlantId);


            var toegewezenaan = "";
            toegewezenaan = ticket.ToegewezenAan[0].ID + ';#' + ticket.ToegewezenAan[0].Naam;
            //    else
            //        toegewezenaan += ';#' + ticket.ToegewezenAan[i].ID + ';#' + ticket.ToegewezenAan[i].Naam;
            //}

            item.set_item("Toegewezen_x0020_aan", toegewezenaan);
            item.set_item("Klant", klantlookupvalue);
            if (ticket.ContactId !== "")
                item.set_item("Contactpersoon", ticket.ContactId);
            item.set_item("Title", ticket.Omschrijving);
            item.set_item("Ordernummer", ticket.Ordernummer);
            item.set_item("Factuurnummer_x0020_C_x0026_P", ticket.FactuurCP);
            item.set_item("Aflevernota", ticket.Aflevernota);
            item.set_item("Afleverdatum", ticket.Afleverdatum);
            item.set_item("Leverancier", ticket.Leverancier);
            item.set_item("Aankoopbonnummer", ticket.Aankoopbonnummer);
            item.set_item("Factuurnummer", ticket.Factuurnummer);
            item.set_item("Type_x0020_ticket", ticket.TypeTicket);
            item.set_item("Status", ticket.Status);
            item.set_item("Opvolgingsdatum", ticket.Opvolgingsdatum);
            item.set_item("Uitgevoerde_x0020_taken", ticket.UitgevoerdeTaken);
            item.set_item("Garantie", ticket.Garantie);
            item.set_item("Kost_x0020_DNV", ticket.KostDNV);
            //item.set_item("Attachments", ticket.Bijlage)
            item.update();
            context.load(item);
            context.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
        }
    }
    this.onQuerySucceeded = function (data) {
        var id = item.get_item('ID');
        be.itc.sp.rest.fileupload.TicketNumber = id;
        var uploadFiles = be.itc.sp.rest.fileupload.uploadFileList();
        uploadFiles.done(function (e) {
            be.itc.sp.rest.fileupload.saveEnabled = true;
            $(".nieuwTicketSaveButton").css({ "pointer-events": "auto" });
            //be.itc.sp.rest.fileupload.showAttachments(be.itc.sp.rest.fileupload.TicketNumber);
        });
        uploadFiles.fail(function (error) {
            be.itc.sp.rest.fileupload.saveEnabled = true;
            $(".nieuwTicketSaveButton").css({ "pointer-events": "auto" });
            be.itc.sp.rest.fileupload.onError(e, "Fout bij het uploaden van bijlagen voor ticket id " + be.itc.sp.rest.fileupload.TicketNumber + ".");
        });
        $('#nieuweTicketPopUp').addClass('Hidden');
        $('.overlay').addClass('Hidden');
        ClearTicketForm();
        klantSelect_Change();
    }

    this.onQueryFailed = function (sender, args) {
        alert('Fout bij het opslaan van het Ticket: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    this.save();
}

function CleanOmschrijving(s) {
    var temptext = s;

    temptext = temptext.replace(/\&/g, '&#38;');
    temptext = temptext.replace(/</g, "&#60;");
    temptext = temptext.replace(/>/g, "&#62;");
    temptext = temptext.replace(/@/g, "&#64;");

    var arrayTaken = temptext.split('\n');
    var result = "<br/>";
    for (var i = 0; i < arrayTaken.length; i++) {
        result += arrayTaken[i] + "<br/>";
    }
    return result;
}

function OpenTicket(ticketID) {
    var url = getQueryStringParameter("SPHostUrl") + '/Lists/Tickets/Dispform.aspx?ID=' + ticketID;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
}

function BewerkTicket(ticketID) {
    var url = getQueryStringParameter("SPHostUrl") + '/Lists/Tickets/Editform.aspx?ID=' + ticketID;// + '&RootFolder=&IsDlg=1';
    window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    //GetTicketById(ticketID, true);
}

function bewerkTicketSaveButton_Click() {
    var ticket = GetBewerkTicketFromForm();
    if (IsTicketValid(ticket)) {
        SaveBewerkTicket(ticket);
    }
    return false;
}

function GetBewerkTicketFromForm() {
    var ticketId = $('#ticketIdHidden').val();
    //var klantid = $("#bewerkTicketKlantSelect").val();
    //var contactid = $("#bewerkTicketContactSelect").val();
    var omschrijving = $("#bewerkTicketOmschrijvingTxt").val();
    var ordernummer = $("#bewerkTicketOrdernummerTxt").val();
    var factuurCP = $("#bewerkTicketFactuurnummerCP").val();
    var aflevernota = $("#bewerkTicketAflevernotaCheckBox").is(':checked') == true;
    var afleverdatum = ToSharePointDate($("#bewerkTicketAfleverdatumTxt").val());
    var aankoopbonnummer = $("#bewerkTicketAankoopbonnummerTxt").val();
    var factuurnummer = $("#bewerkTicketFacuurnummerTxt").val();
    var toegewezenaan = [];
    if ($('#bewerkTicketToegewezenaanSelect :selected'))
        toegewezenaan.push({ ID: $('#bewerkTicketToegewezenaanSelect :selected').val(), Naam: $('#bewerkTicketToegewezenaanSelect :selected').text() });
    var typeTicket = $("#bewerkTicketTypeSelect").val();
    var taken = tinyMCE.get('bewerkTicketUitgevoerdetakenTArea').getContent({ format: 'raw' });
    taken = CleanText(taken);
    var opvolgdatum = ToSharePointDate($("#bewerkTicketopvolgingsdatumTxt").val());
    var status = $("#bewerkTicketStatusSelect").val();
    var garantie = $("#bewerkTicketGarantieSelect").val();
    var kost = $("#bewerkTicketKostTxt").val();
    var leverancier = $("#bewerkTicketLeverancierTxt").val();
    ticket = {
        ID: ticketId,
        //KlantId: klantid,
        //ContactId: contactid,
        Omschrijving: omschrijving,
        Ordernummer: ordernummer,
        FactuurCP: factuurCP,
        Aflevernota: aflevernota,
        Afleverdatum: afleverdatum,
        Aankoopbonnummer: aankoopbonnummer,
        Factuurnummer: factuurnummer,
        TypeTicket: typeTicket,
        UitgevoerdeTaken: taken,
        ToegewezenAan: toegewezenaan,
        Opvolgingsdatum: opvolgdatum,
        Status: status,
        Garantie: garantie,
        KostDNV: kost,
        Leverancier: leverancier
    }
    return ticket;
}

function SaveBewerkTicket(ticket) {
    this.save = function () {
        var item;

        var toegewezenAan = ticket.ToegewezenAan[0].ID + ';#' + ticket.ToegewezenAan[0].Naam;
        var hostWebContext = new SP.AppContextSite(context, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tickets');
        item = list.getItemById(ticket.ID);

        var klantlookupvalue = new SP.FieldLookupValue();
        klantlookupvalue.set_lookupId(ticket.KlantId);

        item.set_item("Toegewezen_x0020_aan", toegewezenAan);
        //item.set_item("Klant", klantlookupvalue);
        //item.set_item("Contact", ticket.ContactId);
        item.set_item("Title", ticket.Omschrijving);
        item.set_item("Ordernummer", ticket.Ordernummer);
        item.set_item("Factuurnummer_x0020_C_x0026_P", ticket.FactuurCP);
        item.set_item("Aflevernota", ticket.Aflevernota);
        item.set_item("Afleverdatum", ticket.Afleverdatum);
        item.set_item("Leverancier", ticket.Leverancier);
        item.set_item("Aankoopbonnummer", ticket.Aankoopbonnummer);
        item.set_item("factuurnummer", ticket.Factuurnummer);
        item.set_item("Type_x0020_ticket", ticket.TypeTicket);
        item.set_item("Status", ticket.Status);
        item.set_item("Opvolgingsdatum", ticket.Opvolgingsdatum);
        item.set_item("Uitgevoerde_x0020_taken", ticket.Taken);
        item.set_item("Garantie", ticket.Garantie);
        item.set_item("Kost_x0020_DNV", ticket.KostDNV);

        item.update();
        context.executeQueryAsync(Function.createDelegate(this, this.onQuerySucceeded), Function.createDelegate(this, this.onQueryFailed));
    }

    this.onQuerySucceeded = function () {
        be.itc.sp.rest.fileupload.TicketNumber = ticket.ID;
        var uploadFiles = be.itc.sp.rest.fileupload.uploadFileList();
        uploadFiles.done(function (e) {
            be.itc.sp.rest.fileupload.saveEnabled = true;
            $(".bewerkTicketSaveButton").css({ "pointer-events": "auto" });
            be.itc.sp.rest.fileupload.showAttachments(be.itc.sp.rest.fileupload.TicketNumber);
        });
        uploadFiles.fail(function (error) {
            be.itc.sp.rest.fileupload.saveEnabled = true;
            $(".bewerkTicketSaveButton").css({ "pointer-events": "auto" });
            be.itc.sp.rest.fileupload.onError(e, "Fout bij het uploaden van bijlagen voor ticket id " + be.itc.sp.rest.fileupload.TicketNumber + ".");
        });
        $('#bewerkTicketPopUp').addClass('Hidden');
    }

    this.onQueryFailed = function () {
        ns.saveEnabled = true;
        $(".bewerkTicketSaveButton").css({ "pointer-events": "auto" });
        alert("Fout bij het opslaan! " + args.get_message() + "\n" + args.get_stackTrace());
    }

    this.save();
}

function CleanText(werken) {
    //var html = /(<([^>]+)>)/ig;
    //werken = werken.replace(html, '');
    var lt = new RegExp('&lt;', 'g');
    werken = werken.replace(gt, "&#62;");
    var gt = new RegExp('&gt;', 'g');
    werken = werken.replace(lt, "&#60;");
    var find = '&amp;';
    var re = new RegExp(find, 'g');
    werken = werken.replace(re, '&#38;');
    //var result = '<p>' + werken + '</p>';
    return werken;
}

function ClearTicketForm() {
    $("#nieuwTicketContactSelect").empty();
    $('#nieuwTicketContactSelect').append("<option value='5'>Dummy Dummy</option>");
    $("#nieuwTicketOmschrijvingTxt").val("");
    $("#nieuwTicketOrdernummerTxt").val("");
    $("#nieuwTicketFactuurnummerCP").val("");
    $("#nieuwTicketAflevernotaCheckBox").val("");
    $("#nieuwTicketAankoopbonnummerTxt").val("");
    $("#nieuwTicketFacuurnummerTxt").val("");
    $('#nieuwTicketToegewezenaanSelect').val([]);
    $("#nieuwTicketTypeSelect").val([]);
    tinyMCE.get('nieuwTicketUitgevoerdetakenTArea').setContent("");
    $("#nieuwTicketStatusSelect").val("Nieuw");
    $("#nieuwTicketGarantieSelect").val([]);
    $("#nieuwTicketKostTxt").val("");
    $("#nieuwTicketLeverancierTxt").val("");
}

function PrintTicket(ticketID) {
    GetTicketByIdToPrint(ticketID);
}

function GetTicketByIdToPrint(ticketID) {

    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tickets');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                + "<Query>"
                + "<Where>"
                + "<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + ticketID + "</Value></Eq>"
                + "</Where>"
                + "</Query>"
                + "<ViewFields>"
                + "<FieldRef Name='Aankoopbonnummer' />"
                + "<FieldRef Name='Afleverdatum' />"
                + "<FieldRef Name='Aflevernota' />"
                + "<FieldRef Name='Contactpersoon' />" //Contact
                + "<FieldRef Name='Factuurnummer' />"   //factuurnummer
                + "<FieldRef Name='Factuurnummer_x0020_C_x0026_P' />"
                + "<FieldRef Name='Garantie' />"
                + "<FieldRef Name='Modified' />"
                + "<FieldRef Name='Editor' />"
                + "<FieldRef Name='ID' />"
                + "<FieldRef Name='Klant' />"
                + "<FieldRef Name='Title' />"
                + "<FieldRef Name='Kost_x0020_DNV' />"
                + "<FieldRef Name='Leverancier' />"
                + "<FieldRef Name='Opvolgingsdatum' />"
                + "<FieldRef Name='Ordernummer' />"
                + "<FieldRef Name='Status' />"
                + "<FieldRef Name='Toegewezen_x0020_aan' />"
                + "<FieldRef Name='Type_x0020_ticket' />"
                //+ "<FieldRef Name='Notities' />"
                + "</ViewFields>"
                + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var ticket = {};
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var klant = item.get_item('Klant').get_lookupValue();
            var myklantid = item.get_item('Klant').get_lookupId();
            var contact = item.get_item('Contactpersoon').get_lookupValue(); //Contact
            var contactid = item.get_item('Contactpersoon').get_lookupId();
            var toegewezen = item.get_item('Toegewezen_x0020_aan');
            var usertext = "";

            for (var i = 0; i < toegewezen.length; i++) {
                var userName = toegewezen[i].get_lookupValue();
                usertext = usertext + userName + '; ';
            }
            var gewijzigddoor = item.get_item('Editor').get_lookupValue();
            var gewijzigddate = item.get_item('Modified').toString();
            var gewijzigd = ToDateString(gewijzigddate, false);
            var opvolgdate = item.get_item('Opvolgingsdatum').toString();
            var opvolg = ToDateString(opvolgdate, true);
            var afleverdate = item.get_item('Afleverdatum').toString();
            var aflever = ToDateString(afleverdate, true);
            var aflevernota = item.get_item('Aflevernota') == true;
            var kost = item.get_item('Kost_x0020_DNV');

            ticket.ID = item.get_item('ID');
            ticket.Omschrijving = item.get_item('Title');
            ticket.Klant = klant;
            ticket.Contact = contact;
            ticket.Ordernummer = item.get_item('Ordernummer');
            ticket.FactuurnummerCP = item.get_item('Factuurnummer_x0020_C_x0026_P');
            ticket.Aflevernota = aflevernota;
            ticket.Afleverdatum = aflever;
            ticket.Aankoopbonnummer = item.get_item('Aankoopbonnummer');
            ticket.Factuurnummer = item.get_item('Factuurnummer');   //factuurnummer
            ticket.Toegewezenaan = usertext;
            ticket.Type = item.get_item('Type_x0020_ticket');
            ticket.Status = item.get_item('Status');
            ticket.Opvolgingsdatum = opvolg;
            //ticket.Uitgevoerdetaken = item.get_item('Notities');
            ticket.Garantie = item.get_item('Garantie');
            ticket.Kost = kost;
            ticket.Leverancier = item.get_item('Leverancier');
        }
        SetPrinterInhoud(ticket);
    }

    self.loadList();
}

function SetPrinterInhoud(ticket) {
    var inhoud = "<br /><br /><fieldset><span>ID: </span><label id='idLabel'>" + ticket.ID + "</label></fieldset>";
    inhoud += "<fieldset><span>Klant: </span><label id='klantLabel'>" + ticket.Klant + "</label></fieldset>";
    inhoud += "<fieldset><span>Contactpersoon: </span><label id='contactpersoonLabel'>" + ticket.Contact + "</label></fieldset>";
    inhoud += "<fieldset><span>Korte omschrijving: </span><label id='omschrijvingLabel'>" + ticket.Omschrijving + "</label></fieldset>";
    inhoud += "<fieldset><span>Toegewezen aan: </span><label id='toegewezenAanLabel'>" + ticket.Toegewezenaan + "</label></fieldset>";
    inhoud += "<fieldset><span>Type: </span><label id='typeLabel'>" + ticket.Type + "</label></fieldset>";
    inhoud += "<fieldset><span>Status: </span><label id='statusLabel'>" + ticket.Status + "</label></fieldset>";
    inhoud += "<fieldset><span>Opvolgdatum: </span><label id='opvolgdatumLabel'>" + (ticket.Opvolgingsdatum == null ? "" : ticket.Opvolgingsdatum ) + "</label></fieldset>";
    inhoud += "<fieldset><span>Garantie: </span><label id='garantieLabel'>" + (ticket.Garantie == null ? "" : ticket.Garantie) + "</label></fieldset>";
    inhoud += "<fieldset><span>Kost DNV: </span><label id='kostLabel'>" + (ticket.Kost == null ? "" : ticket.Kost) + "</label></fieldset>";
    inhoud += "<div style='border:1px solid Black; padding:5px; margin:5px 0;'>";
    inhoud += "<fieldset><span>Ordernummer: </span><label id='ordernummerLabel'>" + (ticket.Ordernummer == null ? "" : ticket.Ordernummer) + "</label></fieldset>";
    inhoud += "<fieldset><span>Factuurnummer C&amp;P: </span><label id='factuurCPLabel'>" + (ticket.FactuurnummerCP == null ? "" : ticket.FactuurnummerCP) + "</label></fieldset>";
    inhoud += "<fieldset><span>Aflevernota: </span><label id='aflevernotaLabel'>" + (ticket.Aflevernota == true ? "Ja" : "Nee") + "</label></fieldset>";
    inhoud += "<fieldset><span>Afleverdatum: </span><label id='afleverdatumLabel'>" + ticket.Afleverdatum + "</label></fieldset>";
    inhoud += "</div>";
    inhoud += "<div style='border:1px solid Black; padding:5px; margin:5px 0;'>";
    inhoud += "<fieldset><span>Leverancier: </span><label id='leverancierLabel'>" + (ticket.Leverancier == null ? "" : ticket.Leverancier) + "</label></fieldset>";
    inhoud += "<fieldset><span>Aankoopbonnummer: </span><label id='aankoopbonnummerLabel'>" + (ticket.Aankoopbonnummer == null ? "" : ticket.Aankoopbonnummer) + "</label></fieldset>";
    inhoud += "<fieldset><span>Factuurnummer: </span><label id='factuurnummerLabel'>" + (ticket.Factuurnummer == null ? "" : ticket.Factuurnummer ) + "</label></fieldset>";
    inhoud += "</div>";
    Popup(inhoud);
}

function Popup(data) {
    var mywindow = window.open('', '', 'height=400,width=600');
    mywindow.document.write('<html><head><title>' + $('#klantselect > option:selected').text() + '</title>');
    mywindow.document.write('<link rel="stylesheet" href="../Content/App.css" type="text/css" />');
    mywindow.document.write("<style>body{font-family:'Century Gothic'; }fieldset{border:none;}span{width:180px;display:inline-block;font-weight:600;}</style>");
    mywindow.document.write('</head><body class="PrintDiv">');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

    return true;
}