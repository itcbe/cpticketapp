﻿'use strict';
var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.exception = be.itc.sp.exception || {};
be.itc.sp.rest = be.itc.sp.rest || {};
be.itc.sp.rest.fileupload = be.itc.sp.rest.fileupload || {};

be.itc.sp.rest.fileupload.addFile = function (inputID) {
    if ($(inputID).val() == "") {
        alert("Je moet eerst een bestand selecteren!");
    } else {
        try {
            var key;
            var pad = $(inputID).val().replace(/C:\\fakepath\\/i, '');
            if (pad.indexOf("\\") != -1) {
                var bestandsnaam = pad.split("\\");
                key = bestandsnaam[bestandsnaam.length - 1];
            } else if (pad.indexOf("/") != -1) {
                var bestandsnaam = pad.split("/");
                key = bestandsnaam[bestandsnaam.length - 1];
            } else {
                key = pad
            }
            if (be.itc.sp.rest.fileupload.fileMap.contains(key)) {
                alert(key + " is al geselecteerd!");
            } else {
                var getFile = be.itc.sp.rest.fileupload.getFileBuffer($(inputID)[0].files[0]);
                getFile.done(function (arrayBuffer) {
                    be.itc.sp.rest.fileupload.fileMap.put(key, arrayBuffer);
                    //be.itc.sp.rest.fileupload.showAttachments(be.itc.sp.rest.fileupload.TicketNumber);
                    be.itc.sp.rest.fileupload.updateFileList();
                });
                getFile.fail(function (error) {
                    be.itc.sp.exception.onError(error, "Fout bij het inlezen van " + key + ".\n")
                });
            }
        } catch (err) {
            alert(err);
        }
    }
};

be.itc.sp.rest.fileupload.deleteFile = function (file) {
    be.itc.sp.rest.fileupload.fileMap.remove(file);
    be.itc.sp.rest.fileupload.updateFileList();
};

be.itc.sp.rest.fileupload.updateFileList = function () {
    ClearFileUpload();
    var fileList = $("#addFileList");
    var ul = $("<ul />");
    fileList.val("");
    fileList.empty();
    for (var i = 0; i++ < be.itc.sp.rest.fileupload.fileMap.size; be.itc.sp.rest.fileupload.fileMap.next()) {
        var key = be.itc.sp.rest.fileupload.fileMap.key();
        var li = $("<li />");
        var spanAttachmentFile = $("<span>" + key + "</span>")
            .attr("class", "attachmentFile");
        var spanFileListControl = $("<span />")
            .attr("class", "fileListControl");
        var verwijder = $('<a>Verwijder</a>')
            .attr("href", "#")
            .attr("onclick", "be.itc.sp.rest.fileupload.deleteFile('" + key + "')")
            .attr("class", "verwijderknop");
        spanFileListControl.append(verwijder);

        li.append(spanAttachmentFile);
        li.append(spanFileListControl);
        ul.append(li);
    }
    fileList.append(ul);
};

be.itc.sp.rest.fileupload.getFileBuffer = function (file) {
    var deferred = jQuery.Deferred();
    var reader = new FileReader();
    reader.onloadend = function (e) {
        deferred.resolve(e.target.result);
    }
    reader.onerror = function (e) {

        deferred.reject(e.target.error);
    }
    reader.readAsArrayBuffer(file);
    return deferred.promise();
};

be.itc.sp.rest.fileupload.getFileInput = function () {
    return $("<input />")
    .attr("id", "getFile")
    .attr("type", "file")
    .attr("size", "50")
    .attr("onchange", "be.itc.sp.rest.fileupload.addFile('#getFile')");
};

be.itc.sp.rest.fileupload.uploadAttachment = function (key) {
    var deferred = $.Deferred();
    var value = be.itc.sp.rest.fileupload.fileMap.get(key);
    var addFile = addFileToFolder(key, value, be.itc.sp.rest.fileupload.TicketNumber);
    addFile.done(function (file, status, xhr) {
        be.itc.sp.rest.fileupload.fileMap.remove(key);
        deferred.resolve(file);
    });
    addFile.fail(function (error) {
        be.itc.sp.exception.onError(error, "Fout bij het opslaan van het bestand.\n");
        deferred.reject(error);
    });
    return deferred.promise();
    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(fileName, arrayBuffer, ticketNumber) {
        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
                "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Tickets')" +
                "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'",
                appweburl, ticketNumber, fileName, hostweburl);

        // Send the request and return the response.
        // This call returns the SharePoint site.
        return jQuery.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-length": arrayBuffer.byteLength
            }
        });
    }
};

be.itc.sp.rest.fileupload.uploadFileList = function () {
    var deferred = $.Deferred();
    var spinner, size, i, j;
    spinner = $("<div id='IOSpinner' class='loadingSpinner'><div class='loadingSpinnerMessage'><h2 id='IOSpinnerTitle' ></h2><img src='../Images/loader_5.gif' alt='spinner' /></div></div>");
    $("body").append(spinner);
    i = 0;
    size = be.itc.sp.rest.fileupload.fileMap.size;
    var files = [];
    for (j = 0; j < be.itc.sp.rest.fileupload.fileMap.size; be.itc.sp.rest.fileupload.fileMap.next()) {
        files[j++] = be.itc.sp.rest.fileupload.fileMap.key();
    }
    uploadFile();
    return deferred.promise();

    function uploadFile() {
        if (i < size) {
            $("#IOSpinnerTitle").html(files[i] + " uploaden...");
            var upload = be.itc.sp.rest.fileupload.uploadAttachment(files[i]);
            upload.done(function (file) {
                i = i + 1;
                uploadFile();
            });
            upload.fail(function (error) {
                deferred.reject(error);
            });
        } else {
            spinner.remove();
            deferred.resolve();
        }
    }
};

be.itc.sp.rest.fileupload.showAttachments = function (id) {
    be.itc.sp.rest.fileupload.TicketNumber = id;
    ClearFileUpload();
    appweburl = appweburl.slice(-1) == "#" ? appweburl.slice(0,-1) : appweburl;
    var scriptbase = hostweburl + "/_layouts/15/";
    $.getScript(scriptbase + "SP.RequestExecutor.js", function () {

        var endpoint = String.format(
                    "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Tickets')" +
                    "/items('{1}')/AttachmentFiles?@target='{2}'",
                appweburl, id, hostweburl);

        $.getScript(scriptbase + "SP.Runtime.js",
            function () {
                $.getScript(scriptbase + "SP.js",
                    function () {
                        $.getScript(scriptbase + "SP.RequestExecutor.js", function () {
                            var context;
                            var factory;
                            var appContextSite;

                            context = new SP.ClientContext(appweburl);
                            factory = new SP.ProxyWebRequestExecutorFactory(appweburl);
                            context.set_webRequestExecutorFactory(factory);
                            appContextSite = new SP.AppContextSite(context, hostweburl);

                            var executor = new SP.RequestExecutor(appweburl);
                            executor.executeAsync({
                                url: endpoint,
                                method: "GET",
                                headers: {
                                    "Accept": "application/json; odata=verbose"
                                },
                                success: function (data) {
                                    var jsonObject = JSON.parse(data.body);
                                    var results = jsonObject.d.results;
                                    var fileList = $("#attachmentList");
                                    var ul = $("<ul />");
                                    for (var i = 0; i < results.length; i++) {
                                        var li = $("<li />");
                                        var spanAttachmentFile = $("<span />")
                                            .attr("class", "attachmentFile");
                                        var link = $('<a>' + results[i].FileName + '</a>')
                                            .attr("href", "https://cpfurnitures.sharepoint.com" + results[i].ServerRelativeUrl)
                                            .attr("target", "_blank");


                                        //var link = $('<a>' + results[i].FileName + '</a>')
                                        //    .attr("href", "https://itcbe.sharepoint.com" + results[i].ServerRelativeUrl)
                                        //    .attr("target", "_blank");
                                        spanAttachmentFile.append(link);
                                        var spanFileListControl = $("<span />")
                                            .attr("class", "fileListControl");
                                        var verwijder = $('<a>Verwijder</a>')
                                            .attr("href", "#")
                                            .attr("onclick", "be.itc.sp.rest.fileupload.deleteAttachment(\"" + results[i].FileName + "\")");
                                        spanFileListControl.append(verwijder);
                                        li.append(spanAttachmentFile);
                                        li.append(spanFileListControl);
                                        ul.append(li);
                                    }
                                    fileList.append(ul);
                                },
                                error: function (error) {
                                    be.itc.sp.exception.onError(error, "Fout bij het inlezen van de attachments.\n");
                                }
                            });
                        }

                        );
                    }
                );
            }
        );
    });
};

be.itc.sp.rest.fileupload.deleteAttachment = function (fileName) {
    var ticketNumber = $("#ticketIdHidden").val();
    var fileCollectionEndpoint = String.format(
            "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Tickets')" +
            "/items({1})/AttachmentFiles/getbyFileName('{2}')?@target='{3}'",
        appweburl, ticketNumber, fileName, hostweburl);

    // Send the request and return the response.
    // This call returns the SharePoint site.
    return jQuery.ajax({
        url: fileCollectionEndpoint,
        type: "POST",
        headers: {
            "accept": "application/json;odata=verbose",
            "X-Http-Method": "DELETE",
            "X-RequestDigest": jQuery("#__REQUESTDIGEST").val()
        },
        success: function () {
            ClearFileUpload();
            be.itc.sp.rest.fileupload.showAttachments(ticketNumber);
            be.itc.sp.rest.fileupload.updateFileList();
        },
        error: function (err) {
            be.itc.sp.exception.onError(err, "Fout bij het verwijderen van bestand: " + fileName + "\n");
        }
    });
};

be.itc.sp.rest.fileupload.saveAttachments = function (ticketNumber) {
    var teller = 0;
    saveAttachment(be.itc.sp.rest.fileupload.fileMap.key(), be.itc.sp.rest.fileupload.fileMap.get(be.itc.sp.rest.fileupload.fileMap.key()), ticketNumber, teller);

    function saveAttachment(key, value, ticketNumber, teller) {
        var addFile = addFileToFolder(key, value);
        addFile.done(function (file, status, xhr) {
            if (teller++ < be.itc.sp.rest.fileupload.fileMap.size) {
                alert("Bestand " + teller + ": " + key + "\n" + value);
                be.itc.sp.rest.fileupload.fileMap.next();
                saveAttachment(be.itc.sp.rest.fileupload.fileMap.key(), be.itc.sp.rest.fileupload.fileMap.get(be.itc.sp.rest.fileupload.fileMap.key()), ticketNumber, teller);

            } else { alert("Alles geüpload!"); }
        });
        addFile.fail(function (error) {
            be.itc.sp.exception.onError(error, "Fout bij het opslaan van het bestand.\n");
        });
    }

    // Add the file to the file collection in the Shared Documents folder.
    function addFileToFolder(fileName, arrayBuffer, ticketNumber) {
        // Construct the endpoint.
        var fileCollectionEndpoint = String.format(
            "{0}/_api/SP.AppContextSite(@target)/web/lists/getbytitle('Tickets')" +
                "/items({1})/AttachmentFiles/add(FileName='{2}')?@target='{3}'" +
            "?@target='{3}'", appweburl, ticketNumber, fileName, hostweburl);

        // Send the request and return the response.
        // This call returns the SharePoint site.
        return jQuery.ajax({
            url: fileCollectionEndpoint,
            type: "POST",
            data: arrayBuffer,
            processData: false,
            headers: {
                "accept": "application/json;odata=verbose",
                "X-RequestDigest": jQuery("#__REQUESTDIGEST").val(),
                "content-length": arrayBuffer.byteLength
            }
        });
    }
};



