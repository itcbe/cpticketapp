﻿'use strict';

var be = be || {};
be.itc = be.itc || {};
be.itc.sp = be.itc.sp || {};
be.itc.sp.editor = be.itc.sp.editor || {};
be.itc.sp.editor.tinymce = be.itc.sp.editor.tinymce || {};
be.itc.sp.exception = be.itc.sp.exception || {};
be.itc.sp.rest = be.itc.sp.rest || {};
be.itc.sp.rest.fileupload = be.itc.sp.rest.fileupload || {};

var fu = be.itc.sp.rest.fileupload;
var tm = be.itc.sp.editor.tinymce;

// jQuery 1.9 is out… and $.browser has been removed – a fast workaround
jQuery.browser = {};
jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase()) && !/trident/.test(navigator.userAgent.toLowerCase());
jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var ddlText, ddlValue, ddl, lblMesg;

fu.fileMap = new Map;
//var itcGroup;
var usercol;
var timer;
var firstTime = true;
//var drempelwaardeTicket, drempelwaardeTijdsregistratie;
var SamenOpslaan = false, UpdateAll = false;
var fileMap = new Map;
var appweburl, hostweburl;

// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    document.getElementById("ctl00_onetidHeadbnnr2").src = "../Images/CPLogo.jpg";
    document.getElementById("ctl00_onetidHeadbnnr2").className = "ms-emphasis";
    fu.saveEnabled = true;
    tm.setEditor();
    $("#mceu_7").css({ "width": "500" });

    $("#zoekKlantZoekTxt").bind("keyup", function () {
        //FilterItems("");
        KlantZoekTxt_KeyUp();
    });

    appweburl = decodeURIComponent(getQueryStringParameter("SPAppWebUrl"));
    hostweburl = decodeURIComponent(getQueryStringParameter("SPHostUrl"));
    $("body").append(tm.getNieuweBijlageDialogForm());
    $("body").append(tm.getBestaandeBijlageDialogForm());
    $("body").append(tm.getAfbeeldingDialogForm());
    $("#nieuweBijlageSubmit")
        .button()
        .click(function (event) {
            event.preventDefault();
            uploadBijlage();
        });
    $("#afbeeldingSubmit")
    .button()
    .click(function (event) {
        event.preventDefault();
        uploadBijlage();
    });
    $("#fileInput").append(fu.getFileInput());
    getUserName();
    setdatePickers();
});

// This function prepares, loads, and then executes a SharePoint query to get the current users information
function getUserName() {
    context.load(user);
    context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
}

// This function is executed if the above call is successful
// It replaces the contents of the 'message' element with the user name
function onGetUserNameSuccess() {
    $('#message').text('Hallo ' + user.get_title());
    GetKlanten();
}

// This function is executed if the above call fails
function onGetUserNameFail(sender, args) {
    alert('Failed to get user name. Error:' + args.get_message());
}

function setdatePickers() {
    $.datepicker.setDefaults($.datepicker.regional["nl"]);
    var date = new Date();

    var datep = $(".datepicker");
    datep.datepicker({
        'dateFormat': 'dd/mm/yy'
    });
    $(".registratiedatepicker").datepicker({ 'dateFormat': 'dd/mm/yy' });
    datep.each(function () {
        var $d = $(this);
        $d.val(ToDateString(date.toString(), true));
    });
}

function OpenKlantPopup() {
    var popup = $("#nieuweKlantPopUp");
    var overlay = $('.overlay');
    if (popup.hasClass('Hidden')) {
        popup.removeClass('Hidden');
        overlay.removeClass('Hidden');
    }
}

function OpenContactPopup() {
    var popup = $("#nieuwContactPopUp");
    var overlay = $('.overlay');
    var klantid = $('#klantselect').val();
    $('#nieuwContactKlantSelect').val(klantid);
    if (popup.hasClass('Hidden')) {
        popup.removeClass('Hidden');
        overlay.removeClass('Hidden');
    }
}

function OpenNieuwTicketPopup() {
    var popup = $("#nieuweTicketPopUp");
    var overlay = $('.overlay');
    var klantid = $('#klantselect').val();
    $('#nieuwTicketKlantSelect').val(klantid);
    nieuwTicketKlantSelect_Change();
    if (popup.hasClass('Hidden')) {
        popup.removeClass('Hidden');
        overlay.removeClass('Hidden');
    }
}

function OpenBewerkTicketPopUp() {
    var popup = $("#bewerkTicketPopUp");
    var overlay = $('.overlay');
    if (popup.hasClass('Hidden')) {
        popup.removeClass('Hidden');
        overlay.removeClass('Hidden');
    }
}

function cancelButton_Click(sender) {
    $(".popup").each(function () {
        var pop = $(this);
        var overlay = $('.overlay');
        if (!pop.hasClass('Hidden')) {
            pop.addClass('Hidden');
            overlay.addClass('Hidden');
        }
    });
    return false;
}
function KlantZoekTxt_KeyUp() {
    var zoektext = $("#zoekKlantZoekTxt").val().toLowerCase();
    FilterItems(zoektext);
}

function CacheItems() {
    ddlText = new Array();
    ddlValue = new Array();
    ddl = document.getElementById("klantselect");
    lblMesg = document.getElementById("zoekResultaatLabel");
    for (var i = 0; i < ddl.options.length; i++) {
        ddlText[ddlText.length] = ddl.options[i].text;
        ddlValue[ddlValue.length] = ddl.options[i].value;
    }
}

function FilterItems(value) {
    ddl.options.length = 0;
    //AddItem("<-- kies een klant -->", 0);

    for (var i = 0; i < ddlText.length; i++) {
        if (ddlText[i].toLowerCase().indexOf(value) != -1) {
            AddItem(ddlText[i], ddlValue[i]);
        }
    }

    if (ddl.options.length == 0) {
        AddItem("Geen klanten gevonden.", "");
        lblMesg.innerHTML = "0 klanten gevonden.";
    }
    else if (ddl.options.length > 1) {
        lblMesg.innerHTML = (ddl.options.length) + " klanten gevonden.";
        if ($("#klantselect")[0].options[0].value != 0) {
            $("#klantselect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
            $("#klantselect")[0].options[0].selected = true;
        }
    }
    else {
        lblMesg.innerHTML = (ddl.options.length) + " klant gevonden.";
        if ($("#klantselect")[0].options[0].value != 0) {
            $("#klantselect").prepend(new Option("<-- Kies een klant -->", 0, true, true));
            $("#klantselect")[0].options[0].selected = true;
        }
    }
    //CurrentKlantId = $("#KlantNummerHiddenField").val();//document.getElementById("").value;
}

function AddItem(text, value) {
    var opt = document.createElement("option");
    opt.text = text;
    opt.value = value;
    ddl.options.add(opt);
}

function ClearFileUpload() {
    $("#attachmentList").empty();
    $("#fileList").empty();
    $("#addFileList").empty();
    $("#fileInput").empty();
    $("#fileInput").append(fu.getFileInput());
}

function BewerkKlant_Click() {
    var klantid = $("#klantselect").val();
    if (klantid != 0) {
        var url = "https://cpfurnitures.sharepoint.com/Lists/Klanten/EditForm.aspx?ID=" + klantid;
        window.open(url, '_blank', 'width=988, height=800,toolbar=no, resizable=yes, location=center, scrollbars=yes, status=no');
    }
}